import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class MainClass {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        String firstNum = scanner.next();
        String secondNum = scanner.next();
        String thirdNum = scanner.next();

//        scanner.close();
//        File file = new File("D:\\r.txt");
        BigNum bigNum = new BigNum();
        String res = bigNum.modularExponentation(firstNum,secondNum,thirdNum);
        System.out.println(res);

    }

}
