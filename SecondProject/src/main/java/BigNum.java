import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/*
GUIDELINE
1-do not put space in a line in file like number = 23
write like this number=23
 */

public class BigNum {

    private static String firstType;
    private static String secondType;
    private static String thirdType;



    private String deletePosAndNegSign(String number) {
        if (number.charAt(0) == '-') {
            number = number.substring(1);
        } else if (number.charAt(0) == '+') {
            number = number.substring(1);
        }
        return number;
    }

    public String sum(String firstNumber, String secondNumber) {

        if (validNumber(firstNumber, secondNumber)) {
            firstNumber = deletePosAndNegSign(firstNumber);
            secondNumber = deletePosAndNegSign(secondNumber);
            int firstNumLength = firstNumber.length();
            int secondNumLength = secondNumber.length();

            String res = "";

            if (firstNumLength >= secondNumLength) {

                if ((firstType.equals("pos") && secondType.equals("pos")) || (firstType.equals("neq") && secondType.equals("neq"))) {

                    res = doSum(firstNumber, secondNumber);

                } else if ((firstType.equals("pos") && secondType.equals("neq")) || (firstType.equals("neq") && secondType.equals("pos"))) {

                    res = doSubtraction(firstNumber, secondNumber);
                }
            } else {

                if ((firstType.equals("pos") && secondType.equals("pos")) || (firstType.equals("neq") && secondType.equals("neq"))) {

                    res = doSum(secondNumber, firstNumber);

                } else if ((firstType.equals("pos") && secondType.equals("neq")) || (firstType.equals("neq") && secondType.equals("pos"))) {

                    String temp = firstType;
                    firstType = secondType;
                    secondType = temp;
                    res = doSubtraction(secondNumber, firstNumber);
                }
            }

            return res;
        } else {
            return "Invalid Inputs";
        }

    }
//overloading sum fun
    public String sum(int firstNumber, int secondNumber) {
        return sum(String.valueOf(firstNumber), String.valueOf(secondNumber));

    }

    private String doSum(String bigNumber, String smallNumber) {
        int c = 0;
        int a = -1;
        int b = -1;


        StringBuilder stringBuilder = new StringBuilder();


        for (int i = 1; i <= bigNumber.length(); i++) {

            a = bigNumber.charAt(bigNumber.length() - i) - 48;

            if (smallNumber.length() - i >= 0) {
                b = smallNumber.charAt(smallNumber.length() - i) - 48;
            } else {
                b = 0;
            }
            int sum = a + b + c;
            stringBuilder.append(sum % 10);
            if (sum >= 10) {
                c = 1;
            } else {
                c = 0;
            }

        }
        if (c == 1) {
            stringBuilder.append(1);
        }
        if (firstType.equals("neq")) {
            stringBuilder.append('-');
        }
        stringBuilder = stringBuilder.reverse();
        int strLength = stringBuilder.length();
        for (int i = 0; i < strLength; i++) {
            if (stringBuilder.charAt(0) == '0') {
                stringBuilder.deleteCharAt(0);
            } else {
                break;
            }
        }

        if (stringBuilder.length() == 0) {
            return "0";
        }
        return stringBuilder.toString();

    }


    //check the validation of numbers
    private boolean validNumber(String firstNumber, String secondNumber) {

        //checking first digit of number valid posbilites(+,-,number)
        if ((firstNumber.charAt(0) >= 48 && firstNumber.charAt(0) <= 57) || firstNumber.charAt(0) == '+' || firstNumber.charAt(0) == '-') {

        } else {
            return false;
        }

        if ((secondNumber.charAt(0) >= 48 && secondNumber.charAt(0) <= 57) || secondNumber.charAt(0) == '+' || secondNumber.charAt(0) == '-') {

        } else {
            return false;
        }

        for (int i = 1; i < firstNumber.length(); i++) {
            if (firstNumber.charAt(i) >= 48 && firstNumber.charAt(i) <= 57) {

            } else {
                return false;
            }
        }

        for (int i = 1; i < secondNumber.length(); i++) {
            if (secondNumber.charAt(i) >= 48 && secondNumber.charAt(i) <= 57) {

            } else {
                return false;
            }

        }


        //checking numbers types(+,-)
        verifyType(firstNumber.charAt(0), secondNumber.charAt(0));
        return true;
    }

    //check the validation of three numbers
    private boolean validModularExponentiation(String firstNumber, String secondNumber, String p) {

        if ((firstNumber.charAt(0) >= 48 && firstNumber.charAt(0) <= 57) || firstNumber.charAt(0) == '+' || firstNumber.charAt(0) == '-') {

        } else {
            return false;
        }

        if ((secondNumber.charAt(0) >= 48 && secondNumber.charAt(0) <= 57) || secondNumber.charAt(0) == '+' || secondNumber.charAt(0) == '-') {

        } else {
            return false;
        }

        if ((p.charAt(0) >= 48 && p.charAt(0) <= 57) || p.charAt(0) == '+' || p.charAt(0) == '-') {

        } else {
            return false;
        }

        for (int i = 1; i < firstNumber.length(); i++) {
            if (firstNumber.charAt(i) >= 48 && firstNumber.charAt(i) <= 57) {

            } else {
                return false;
            }
        }

        for (int i = 1; i < secondNumber.length(); i++) {
            if (secondNumber.charAt(i) >= 48 && secondNumber.charAt(i) <= 57) {

            } else {
                return false;
            }

        }
        for (int i = 1; i < p.length(); i++) {
            if (p.charAt(i) >= 48 && p.charAt(i) <= 57) {

            } else {
                return false;
            }

        }

        verifyModularTypes(firstNumber.charAt(0), secondNumber.charAt(0), p.charAt(0));
        return true;
    }

    public String subtraction(String firstNumber, String secondNumber) {
        if (validNumber(firstNumber, secondNumber)) {

            firstNumber = deletePosAndNegSign(firstNumber);
            secondNumber = deletePosAndNegSign(secondNumber);
            int firstNumLength =firstNumber.length();
            int secondNumLength = secondNumber.length();

            if (firstType.equals("pos") && secondType.equals("pos")) {


                if (firstNumLength >= secondNumLength) {
                    firstType = "pos";
                    secondType = "neq";
                    return doSubtraction(firstNumber, secondNumber);
                } else {
                    firstType = "neq";
                    secondType = "pos";
                    return doSubtraction(secondNumber, firstNumber);
                }

            } else if (firstType.equals("neq") && secondType.equals("neq")) {


                if (firstNumLength >= secondNumLength) {
                    firstType = "neq";
                    secondType = "pos";
                    return doSubtraction(firstNumber, secondNumber);
                } else {
                    firstType = "pos";
                    secondType = "neq";
                    return doSubtraction(secondNumber, firstNumber);
                }

            } else if (firstType.equals("pos") && secondType.equals("neq")) {

                secondType = "pos";
                if (firstNumLength >= secondNumLength) {
                    return doSum(firstNumber, secondNumber);
                } else {
                    return doSum(secondNumber, firstNumber);
                }
            } else if (firstType.equals("neq") && secondType.equals("pos")) {
                secondType = "neq";

                if (firstNumLength >= secondNumLength) {
                    return doSum(firstNumber, secondNumber);
                } else {
                    return doSum(secondNumber, firstNumber);
                }
            }
        }
        return "Invalid inputs";
    }

    public String subtraction(int firstNumber, int secondNumber) {
        return subtraction(String.valueOf(firstNumber), String.valueOf(secondNumber));
    }

    private String doSubtraction(String bigNum, String smallNum) {
        String res = "0";


        if (smallNum.length() == bigNum.length()) {
            int counter = 0;

            while (counter < smallNum.length()) {
                if ((smallNum.charAt(counter) - 48) > (bigNum.charAt(counter) - 48)) {
                    String temp = bigNum;
                    bigNum = smallNum;
                    smallNum = temp;
                    temp = firstType;
                    firstType = secondType;
                    secondType = temp;
                    break;

                } else if ((bigNum.charAt(counter) - 48) > (smallNum.charAt(counter) - 48)) {
                    break;
                }
                counter++;
            }


            if (counter == smallNum.length()) {
                return res;
            }
        }


        StringBuilder stringBuilder = new StringBuilder();
        int flag = 0;
        for (int i = 1; i <= bigNum.length(); i++) {
            int bigLast = bigNum.charAt(bigNum.length() - i) - 48;
            int smallLast;
            //safe boundary for small number
            if ((smallNum.length() - i) < 0) {
                smallLast = 0;
            } else {
                smallLast = smallNum.charAt(smallNum.length() - i) - 48;

            }
            bigLast -= flag;
            if (bigLast - smallLast >= 0) {

                if (bigLast - smallLast == 0 && i == bigNum.length()) {

                } else {
                    stringBuilder.append(bigLast - smallLast);

                }
                flag = 0;

            } else {
                bigLast += 10;
                stringBuilder.append(bigLast - smallLast);
                flag = 1;

            }
        }
        if (firstType.equals("pos")) {

        } else {
            stringBuilder.append("-");
        }
        return stringBuilder.reverse().toString();

    }

    public String multiplication(String firstNumber, String secondNumber) {

        if (validNumber(firstNumber, secondNumber)) {
            firstNumber = deletePosAndNegSign(firstNumber);
            secondNumber = deletePosAndNegSign(secondNumber);
            int firstNumLength = firstNumber.length();
            int secondNumLength = secondNumber.length();
            if ((firstType.equals("pos") && secondType.equals("pos")) || firstType.equals("neq") && secondType.equals("neq")) {
                if (firstNumLength >= secondNumLength) {
                    //1->pos    0->neq
                    return doMultiply(firstNumber, secondNumber, 1);
                } else {
                    return doMultiply(secondNumber, firstNumber, 1);
                }
            } else {

                if (firstNumLength >= secondNumLength) {
                    //1->pos    0->neq
                    return doMultiply(firstNumber, secondNumber, 0);
                } else {
                    return doMultiply(secondNumber, firstNumber, 0);
                }

            }
        }
        return "Invalid inputs";
    }

    public String multiplication(int firstNumber, int secondNumber) {
        return multiplication(String.valueOf(firstNumber), String.valueOf(secondNumber));
    }

    private String doMultiply(String bigNum, String smallNum, int type) {

        int counter = 0;
        String first = "0";
        String second = "0";
        for (int i = smallNum.length() - 1; i >= 0; i--) {
            StringBuilder stringBuilder = new StringBuilder();
            int num = 0;


            for (int j = bigNum.length() - 1; j >= 0; j--) {
                //putting zero
                if (j == bigNum.length() - 1) {
                    for (int c = 0; c < counter; c++) {
                        stringBuilder.append("0");
                    }
                }
                int temp = (smallNum.charAt(i) - 48) * (bigNum.charAt(j) - 48) + num;
                num = temp / 10;
                if (j == 0) {
                    stringBuilder.append(temp % 10);
                    if (temp >= 10) {
                        stringBuilder.append(temp / 10);
                    }
                } else {
                    stringBuilder.append(temp % 10);

                }

            }
            counter++;
            second = stringBuilder.reverse().toString();
            first = sum(first, second);
        }

        if (type == 0) {
            first = "-" + first;
        }
        return first;
    }

    public String division(String firstNumber, String secondNumber) {
        if (validNumber(firstNumber, secondNumber)) {


            if ((firstType.equals("pos") && secondType.equals("pos")) || firstType.equals("neq") && secondType.equals("neq")) {

                return doDivision(deletePosAndNegSign(firstNumber), deletePosAndNegSign(secondNumber));

            } else {

                String res = doDivision(deletePosAndNegSign(firstNumber), deletePosAndNegSign(secondNumber));
                return "-" + res;

            }
        }

        return "Invalid Inputs";
    }

    public String division(int firstNumber, int secondNumber) {
        return division(String.valueOf(firstNumber), String.valueOf(secondNumber));
    }

    private String doDivision(String firstNum, String secondNum) {

        String count = "0";
        StringBuilder stringBuilder = new StringBuilder();
        String first = "0";
        int flag2 = 0;
        if (!firstNum.equals("0") && secondNum.equals("0")) {
            return firstNum+" cannot divide by zero";
        }else if(firstNum.equals("0")){
            return "0";
        }
        for (int i = 0; i < firstNum.length(); i++) {


            first = String.valueOf(doSum(doMultiply(count, "10", 1), String.valueOf((firstNum.charAt(i) - 48))));

            int counter = 0;
            while (isBigger(first, secondNum)) {
                first = doSubtraction(first, secondNum);
                counter++;
            }
            count = first;

            stringBuilder.append(counter);



                if (i + 1 == firstNum.length() && flag2 == 0 && !first.equals("0")) {

                    stringBuilder.append(".");
                    firstNum = firstNum + "00";

                    flag2 = 1;
                }


        }

        int stringLentgh = stringBuilder.length();
        for (int i = 0; i < stringLentgh; i++) {
            if (stringBuilder.charAt(0) == '0'&&stringBuilder.charAt(1)!='.' ) {
                if(i+1!=stringLentgh){
                    stringBuilder.deleteCharAt(0);
                }
            } else {
                break;
            }
        }

        return stringBuilder.toString();


    }


    private boolean isBigger(String first, String second) {
        if (first.length() < second.length()) {
            return false;
        } else if (first.length() > second.length()) {
            return true;
        }
        for (int i = 0; i < second.length(); i++) {
            int ch1 = first.charAt(i) - 48;
            int ch2 = second.charAt(i) - 48;
            if (ch1 > ch2) {
                return true;
            } else if (ch1 == ch2 && i + 1 == second.length()) {
                return true;
            } else if (ch1 < ch2) {
                return false;
            }
        }

        return false;
    }

    public String modularExponentation(String firstNumber, String secondNumber, String p) {
        String res = null;
        String first;
        String second;
        String third;
        if (validModularExponentiation(firstNumber, secondNumber, p) && !p.equals("0")) {
            first = firstType;
            second = secondType;
            third = thirdType;
            firstNumber = deletePosAndNegSign(firstNumber);
            secondNumber = deletePosAndNegSign(secondNumber);
             p = deletePosAndNegSign(p);
            res = doModularExponentiation(firstNumber, secondNumber, p);

        } else {
            return "invalid inputs";
        }
        if (second.equals("neq")) {
            return res;
        }
        //if the result of power and p iteself is positive
        else if ((first.equals("pos") || (first.equals("neq") &&
                mod(secondNumber,"2").equals("0"))) && third.equals("pos")) {
            return res;
        }
        //if the result of power is negative and p is positive
        else if ((first.equals("neq") && mod(secondNumber, "2").equals("1")) &&
                third.equals("pos")) {
            firstType = "pos";
            secondType = "neq";
            if(res.equals("0")){
                return res;
            }else{
                return doSubtraction(p, res);
            }
        } else if (first.equals("pos") && third.equals("neq")) {
            firstType = "pos";
            secondType = "neq";
            if(res.equals("0")){
                return res;
            }else{
                return doSubtraction(res, p);
            }
        } else if (first.equals("neq") && third.equals("neq")) {
            return "-" + res;
        }


        return res;
    }

    public String modularExponentation(int firstNumber, int secondNumber, int p) {
        return modularExponentation(String.valueOf(firstNumber), String.valueOf(secondNumber), String.valueOf(p));
    }

    private String doModularExponentiation(String firstNumber, String secondNumber, String p) {

        String res;

        if (firstNumber.equals("0")) {
            return "0";
        }
        if (secondNumber.equals("0")) {
            if (p.equals("1")) {
                return "0";
            } else {
                return "1";
            }
        }

        if (secondType.equals("neq")) {
            String firstTemp = firstNumber;
            if(secondNumber.equals("1")){
                return doDivision("1",firstNumber);
            }
            firstNumber = doMultiply(firstNumber, firstTemp, 1);
            while (!secondNumber.equals("2")) {
                firstNumber = doMultiply(firstNumber, firstTemp, 1);
                secondNumber = doSubtraction(secondNumber, "1");
                if(firstNumber.length()>4){
                    return "0.00";
                }
            }
            if (mod(secondNumber, "2").equals("1")) {
                firstNumber = doMultiply(firstNumber, firstTemp, 1);
            }
            return doDivision("1", firstNumber);
        }

        //second number is odd
        if (mod(secondNumber, "2").equals("1")) {
            res = mod(firstNumber, p);
            secondNumber = doSubtraction(secondNumber, "1");
            res = mod(doMultiply(res, mod(doModularExponentiation(firstNumber, secondNumber, p), p), 1), p);
        }
        //second number is even
        else {
            secondNumber = doDivision(secondNumber, "2");
            res = doModularExponentiation(firstNumber, secondNumber, p);
            res = mod(doMultiply(res, res, 1), p);
        }
        return mod(res, p);


    }


    public String fileInput(File file) throws FileNotFoundException {

        Scanner scanner = new Scanner(file);

        ArrayList<String> valueArr = new ArrayList<>();
        ArrayList<String> keyArr = new ArrayList<>();
        String lastLineStr = "";
        while (scanner.hasNextLine()) {
            String[] keyValue = scanner.nextLine().split("=");
            if (keyValue.length == 2) {
                keyArr.add(keyValue[0]);
                valueArr.add(keyValue[1]);
            } else {
                lastLineStr = keyValue[0];
                break;
            }

        }
        ArrayList<Character> sign = new ArrayList<>();
        for (int i = 0; i < lastLineStr.length(); i++) {
            if (lastLineStr.charAt(i) == '+' || lastLineStr.charAt(i) == '-' || lastLineStr.charAt(i) == '*' ||
                    lastLineStr.charAt(i) == '/') {
                sign.add(lastLineStr.charAt(i));
            }
        }
        String[] keyValue = lastLineStr.split("[-*/+]");
        ArrayList<String> keyValueList = new ArrayList<>(Arrays.asList(keyValue));
        ArrayList<String> keyArrCopy = new ArrayList<>();
        ArrayList<String> valueArrCopy = new ArrayList<>(valueArr);
        int resIndex=0;
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < sign.size(); j++) {
                String key = keyValueList.get(j);
                String secondKey = keyValueList.get(j+1);
                int index = keyArr.indexOf(key);
                int secondIndex = keyArr.indexOf(secondKey);
                if (i == 0) {
                    if (sign.get(j) == '*') {
                        String secondNum = valueArr.get(secondIndex);
                        //if there is reputation in variables in last line
                        if(!keyArrCopy.contains(secondKey)){
                            keyArrCopy.add(secondKey);
                        }else{
                            secondNum = valueArrCopy.get(secondIndex);

                        }
                        String result = multiplication(valueArr.get(index),secondNum );
                        keyValueList.remove(j);
                        valueArr.remove(secondIndex);
                        valueArr.add(secondIndex,result);
                        sign.remove(j);
                        --j;


                    } else if (sign.get(j) == '/') {
                        String secondNum = valueArr.get(secondIndex);
                        //if there is reputation in variables in last line
                        if(!keyArrCopy.contains(secondKey)){
                            keyArrCopy.add(secondKey);
                        }else{
                            secondNum = valueArrCopy.get(secondIndex);

                        }
                        String result = division(valueArr.get(index),secondNum );
                        String[] arr = result.split("\\.");
                        result = arr[0];
                        keyValueList.remove(j);
                        valueArr.remove(secondIndex);
                        valueArr.add(secondIndex,result);
                        sign.remove(j);
                        --j;
                    }
                } else {
                    if (sign.get(j) == '+') {
                        String secondNum = valueArr.get(secondIndex);
                        //if there is reputation in variables in last line
                        if(!keyArrCopy.contains(secondKey)){
                            keyArrCopy.add(secondKey);
                        }else{
                            secondNum = valueArrCopy.get(secondIndex);

                        }
                        String result = sum(valueArr.get(index),secondNum );
                        keyValueList.remove(j);
                        valueArr.remove(secondIndex);
                        valueArr.add(secondIndex,result);
                        sign.remove(j);
                        --j;
                    } else if (sign.get(j) == '-') {
                        String secondNum = valueArr.get(secondIndex);
                        //if there is reputation in variables in last line
                        if(!keyArrCopy.contains(secondKey)){
                            keyArrCopy.add(secondKey);
                        }else{
                            secondNum = valueArrCopy.get(secondIndex);

                        }
                        String result = subtraction(valueArr.get(index),secondNum );
                        keyValueList.remove(j);
                        valueArr.remove(secondIndex);
                        valueArr.add(secondIndex,result);
                        sign.remove(j);
                        --j;
                    }
                }

            resIndex = secondIndex;
            }
        }

        return valueArr.get(resIndex);


    }


    private String mod(String dividend, String divisor) {
        String remainder = "0";
        String count = "0";
        for (int i = 0; i < dividend.length(); i++) {
            remainder = doSum(doMultiply(count, "10", 1), String.valueOf(dividend.charAt(i) - 48));
            while (isBigger(remainder, divisor)) {
                remainder = doSubtraction(remainder, divisor);
            }
            count = remainder;
        }
        return remainder;
    }

    private static void verifyType(char num1, char num2) {
        if (num1 == '-') {
            firstType = "neq";
        } else {
            firstType = "pos";
        }

        if (num2 == '-') {
            secondType = "neq";
        } else {
            secondType = "pos";
        }
    }

    private void verifyModularTypes(char num1, char num2, char p) {
        if (num1 == '-') {
            firstType = "neq";
        } else {
            firstType = "pos";
        }

        if (num2 == '-') {
            secondType = "neq";
        } else {
            secondType = "pos";
        }

        if (p == '-') {
            thirdType = "neq";
        } else {
            thirdType = "pos";
        }
    }

}
